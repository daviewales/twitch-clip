# Twitch Clip

Clip videos of any length from Twitch (and anywhere else supported by youtube-dl...)

## Dependencies

You will need:

- bash
- youtube-dl
- ffmpeg

## Install

Just copy `twitch-clip.sh` somewhere and make it executable.
For bonus points, put a symbolic link in your `$PATH`!

## Usage

Run `twitch-clip.sh` without arguments for help.
e.g.

``` sh
    Usage: twitch-clip URL START DURATION OUTPUT_NAME
    The clip will be saved in the current directory
```

`URL` is the video url.
`START` and `DURATION` are the start and duration of the clip you want in the format 'hh:mm:ss'.
`OUTPUT_NAME` is the name of the output file. The file extension will be automatically added.

Complete example:

``` sh
twitch-clip.sh https://www.twitch.tv/videos/899083175 00:00:00 00:05:00 test-video
```

This will download the first five minutes of the linked video, and save it in the file `test-video.mp4` (assuming that mp4 is the correct format!)



#!/usr/bin/env sh

URL=$1
#FORMAT=$2 # We can just default to the best available for now
START=$2
DURATION=$3
OUTPUT=$4

if [ -z "$URL" ] ; then
    echo 'Usage: twitch-clip URL START DURATION OUTPUT_NAME';
    #echo 'If only the URL is provided, the format options will be printed.'
    #echo 'Choose one of the listed, and run again with the start time (hh:mm:ss)'
    #echo 'and the duration (hh:mm:ss).'
    echo 'The clip will be saved in the current directory'
#elif [ -z "$FORMAT" ] ; then
#    youtube-dl -F $URL
elif [ ! -z "$OUTPUT" ] ; then
    EXTENSION=$(youtube-dl -F $URL | grep best | awk ' { print $2 }')
    echo "URL: $URL\nSTART: $START\nDURATION: $DURATION\nOUTPUT: $OUTPUT.$EXTENSION"
    DIRECT_URL=$(youtube-dl --get-url $URL)
    ffmpeg -ss $START -i $DIRECT_URL -t $DURATION -c:v copy -c:a copy "$OUTPUT.$EXTENSION"
fi


#youtube-dl -f 1080p60 --get-url https://www.twitch.tv/videos/889845008

#ffmpeg -ss 3:44:28 -i https://d2nvs31859zcd8.cloudfront.net/c132178669e49090c023_bandersaur_40337990349_1611734688/chunked/index-dvr.m3u8 -t 0:07:00 -c:v copy -c:a copy test.mp4
